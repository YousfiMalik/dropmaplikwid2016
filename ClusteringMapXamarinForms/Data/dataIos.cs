﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms.GoogleMaps;

namespace ClusteringMapXamarinForms
{
	public class dataIos
	{
		
		public Collection<CustomPin> Items { get; set; }
		public dataIos()
		{
			Items = new Collection<CustomPin>();
			var Latitude = 40.741895;
			var Longitude = -73.989308;

			for (int i = 0; i < 5; ++i)
			{
				var CurrentPin = new CustomPin();
				var t = i * System.Math.PI * 0.33f;
				var r = 0.005 * System.Math.Exp(0.1 * t);
				var x = r * System.Math.Cos(t);
				var y = r * System.Math.Sin(t);
				CurrentPin.Id = "Xamarin";
				CurrentPin.Pin = new Pin
				{
					Position = new Position(Latitude + x, Longitude + y),
					Label = "Hi",
					Address = "lac 1 ,cogite"
				};
				CurrentPin.Url = i.ToString();

				Items.Add(CurrentPin);
			}
		
		}
	}
}
