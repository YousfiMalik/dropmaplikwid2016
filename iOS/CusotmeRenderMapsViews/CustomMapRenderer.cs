﻿using System;
using ClusteringMapXamarinForms;
using ClusteringMapXamarinForms.iOS;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.GoogleMaps.iOS;
using Google.Maps;
using Xamarin.Forms.Maps;
using UIKit;
using System.Collections.Generic;
using GMCluster;
using Foundation;
using CoreGraphics;
using CoreAnimation;
using System.Drawing;
using ClusteringMapXamarinForms.CustomFormElements;
using CoreFoundation;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
//[assembly: ExportRendererAttribute(typeof(ModalHostPage), typeof(ModalHostPageRenderer))]
namespace ClusteringMapXamarinForms.iOS
{
	public class CustomMapRenderer : MapRenderer, IGMUClusterManagerDelegate, IMapViewDelegate 

	{
		//UIView customPinView;
		//List<CustomPin> customPins;
		const int kClusterItemCount =250;
		const double kCameraLatitude = 41.894708;
		const double kCameraLongitude = 12.492523;
        const double extent = 0.2;
		MapView map;
		public int i = 0;
		UIImage img = new UIImage();
		UIButton btnFade = new UIButton();
		public static Details dtPage = null;
		GMUClusterManager clusterManager;



		protected override void OnElementChanged(ElementChangedEventArgs<View> e)
		{

			base.OnElementChanged(e);
			if (e.OldElement != null)
			{
				map = Control as MapView;
				map.UserInteractionEnabled = true;
				AddCluster();
				var camera = CameraPosition.FromCamera(kCameraLatitude, kCameraLongitude, 20);
				map = MapView.FromCamera(CGRect.Empty, camera);
				map.MyLocationEnabled = true;
			
			}

			if (e.NewElement != null)
			{
				var formsMap = (CustomMap)e.NewElement;
				map = Control as MapView;

				//map.UserInteractionEnabled = true;
				AddCluster();
				var camera = CameraPosition.FromCamera(kCameraLatitude, kCameraLongitude, 20);
				map = MapView.FromCamera(CGRect.Empty, camera);
				map.MyLocationEnabled = true;
				//	AddCluster();
			}
		}
			
		public UIImage ResizeImage(UIImage sourceImage, float width, float height)
		{ 
			UIGraphics.BeginImageContext(new SizeF(width, height));
			sourceImage.Draw(new RectangleF(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

     void AddCluster ()
    {
      var googleMapView = map; //use the real mapview init'd somewhere else instead of this
      var iconGenerator = new GMUDefaultClusterIconGenerator ();
      var algorithm = new GMUNonHierarchicalDistanceBasedAlgorithm ();
      var renderer = new GMUDefaultClusterRenderer (googleMapView, iconGenerator);
    
       renderer.WeakDelegate = this;
    
      clusterManager = new GMUClusterManager (googleMapView, algorithm, renderer);

			for (int j = 1; j <= 10; j++)
       {
          var lat = kCameraLatitude + extent * GetRandomNumber (-1.0, 1.0);
        
          var lng = kCameraLongitude + extent * GetRandomNumber (-1.0, 1.0);
        
          var name = $"Item {j}";
				IGMUClusterItem item;

				if (j % 2 == 0)
					item = new DropInMap(j, "Photo", (float)lat + 0.2f, (float)lng + 0.2f, "pic1", "race", DateTime.Now);
				else 

					item = new DropInMap(j, "Challenge", (float)lat + 0.2f, (float)lng + 0.2f, "pic2", "Challenge", DateTime.Now);
				
				//IGMUClusterItem item = new DropInMap(lat, lng, name);
          clusterManager.AddItem (item);

        }
    			
    		clusterManager.Cluster();
    
  		    clusterManager.SetDelegate (this, this);
}


		public double GetRandomNumber(double minimum, double maximum)
		{
			Random random = new Random();
			return random.NextDouble() * (maximum - minimum) + minimum;
		}

		public int GetRandomNumberPic(int minimum, int maximum)
		{
			Random random = new Random();
			return random.Next() * (maximum - minimum) + minimum;
		}

		public static UIImage DrawText(UIImage uiImage, string sText, UIColor textColor, int iFontSize)
        {
            nfloat fWidth = uiImage.Size.Width;
            nfloat fHeight = uiImage.Size.Height;

            CGColorSpace colorSpace = CGColorSpace.CreateDeviceRGB();

            using (CGBitmapContext ctx = new CGBitmapContext(IntPtr.Zero, (nint)fWidth, (nint)fHeight, 8, 4 * (nint)fWidth, CGColorSpace.CreateDeviceRGB(), CGImageAlphaInfo.PremultipliedFirst))
            {
                ctx.DrawImage(new CGRect(0, 0, (double)fWidth, (double)fHeight), uiImage.CGImage);

                ctx.SelectFont("Helvetica", iFontSize, CGTextEncoding.MacRoman);

               
                float start, end, textWidth;

               
                start = (float)ctx.TextPosition.X;
               
                ctx.SetTextDrawingMode(CGTextDrawingMode.Invisible);
                
                ctx.ShowText(sText);
               
                end = (float)ctx.TextPosition.X;
               
                textWidth = end - start;

                nfloat fRed;
                nfloat fGreen;
                nfloat fBlue;
                nfloat fAlpha;
               
                textColor.GetRGBA(out fRed, out fGreen, out fBlue, out fAlpha);
                ctx.SetFillColor(fRed, fGreen, fBlue, fAlpha);

               
                ctx.SetTextDrawingMode(CGTextDrawingMode.Fill);

               
                ctx.ShowTextAtPoint(8, 17, sText);


                return UIImage.FromImage(ctx.ToImage());
            }
        }

	
		[Export("renderer:willRenderMarker:")]
		public  void WillRenderMarker(GMUClusterRenderer renderer, Overlay marker)
		{
			if (marker is Marker)
			{
				var myMarker = (Marker)marker;
				if (myMarker.UserData is DropInMap)
				{
					Console.WriteLine("1!");
					DropInMap myDrop = (DropInMap)myMarker.UserData;
					DropViewOnMap mdvc = new DropViewOnMap(myDrop.id,myDrop.type, myDrop.thumbnail, myDrop.emoji);
					Console.WriteLine("2!");
					//MyDropView mdvc = new MyDropView();

					//myMarker.IconView = new UIView();
					//myMarker.IconView = mdvc.getView();

					myMarker.IconView = new UIView();
					//myMarker.IconView.BackgroundColor = Color.Black.ToUIColor();
					Console.WriteLine("3!");
					myMarker.IconView = mdvc.getView();
					Console.WriteLine("Done!");

				}

			}


		}

		[Export("mapViewDidStartTileRendering:")]
		public bool mapViewDidStartTileRendering(MapView mapView)
		{
			
			map = mapView;

			return true;
		}

		[Export("mapView:didTapMarker:")]
		public bool TappedMarker(MapView mapView, Marker marker)
		{
		/*	BoxView bxImg = new BoxView();
			btnFade.RemoveFromSuperview();
			UIView imgView = new UIView();
			//UIImage image = new UIImage();
			var newCamera = CameraPosition.FromCamera(marker.Position.Latitude+0.05f,marker.Position.Longitude, map.Camera.Zoom + 6f);
			btnFade = UIButton.FromType(UIButtonType.Custom);
			btnFade.RemoveFromSuperview();
			var update = CameraUpdate.SetCamera(newCamera);
			mapView.MoveCamera(update);
			btnFade.RemoveFromSuperview();
				btnFade.SetImage(UIImage.FromFile("Cogite.png"), UIControlState.Normal);
				btnFade.Frame = new CGRect(1, 3, UIScreen.MainScreen.Bounds.Width - 200f, UIScreen.MainScreen.Bounds.Height - 200f);
				btnFade.TouchUpInside += delegate
				{
					btnFade.Hidden = true;
				} ;
				
			imgView.UserInteractionEnabled = true;
			btnFade.UserInteractionEnabled = true;
			//image.Frame = new CoreGraphics.CGRect(10, 10, UIScreen.MainScreen.Bounds.Width - 50f, UIScreen.MainScreen.Bounds.Height - 300f);
			//imgView.Add(btnFade);
			imgView.IsAccessibilityElement = true;
			imgView.AddSubview(btnFade);
			btnFade.IsAccessibilityElement = true;
			imgView.AddSubview(btnFade);
			//imgView.AddSubview(image);
			//map.MoveCamera(update);
			//btnFade.AccessibilityViewIsModal = true;
			mapView.AddSubview(btnFade);
			//this.DisplayPageModalRequested += OnDisplayPageModalRequested;

		/*var newCamera = CameraPosition.FromCamera(marker.Position.Latitude + 0.05f, marker.Position.Longitude, map.Camera.Zoom + 6f);
			var update = CameraUpdate.SetCamera(newCamera);
			mapView.MoveCamera(update);
			dtPage = new Details();
			Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(dtPage);
			//    await Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(new MapView());*/
			dtPage = new Details();
			DropInMap myDropView = (DropInMap)marker.UserData;
			dtPage.setImage(myDropView.thumbnail);
			Xamarin.Forms.Application.Current.MainPage.Navigation.PushModalAsync(dtPage);
			return true;
		}

		public override void TouchesBegan(NSSet touches, UIEvent evt)
		{

			base.TouchesBegan(touches, evt);

			// If Multitouch is enabled, report the number of fingers down


			// Get the current touch
			/*UIView view = touches.AnyObject as UIView;
			if (view != null)
			{
				UIAlertView alert = new UIAlertView()
				{
					Title = "alert title",
					Message = "this is a simple alert"
				} ;
				alert.AddButton("OK");
				alert.Show();
			}*/
		}

		/*	public override void TouchesBegan(NSSet touches, UIEvent evt)
			{
				base.TouchesBegan(touches, evt);

			} */

		/*	UIView markerInfoWindow(UIView view, Marker marker)
			
				View infoWindow = Google.Maps.InfoWindow;

				var renderer = GetRenderer(infoWindow);
				var layoutWidth = UIScreen.MainScreen.Bounds.Width * 0.9;
				var layoutHeight = UIScreen.MainScreen.ScreenSize.Bounds.Height * 0.39;
				renderer.NativeView.Bounds = new CGRect(0, 0, (nfloat)layoutWidth, (nfloat)layoutHeight);
				renderer.NativeView.BackgroundColor = Color.White.ToUIColor();
				renderer.NativeView.SetNeedsDisplay();

				return renderer.NativeView;
			} */
		
	[Export("mapView:didBeginDraggingMarker:")]
	public void didBeginDraggingMarker(MapView mapView, Marker marker)
		{
		UIAlertView alert = new UIAlertView()
		{
			Title = "alert title",
			Message = "this is a simple alert"
		};
			alert.AddButton("OK");
			alert.Show();
		if (marker is Marker)
			{
				var myMarker = (Marker)marker;
				myMarker.Icon = UIImage.FromBundle("clustermarker");
		}
		}

		[Export("clusterManager:didTapCluster:")]
		public void DidTapCluster(GMUClusterManager clusterManager, IGMUCluster cluster)
		{
			var newCamera = CameraPosition.FromCamera(cluster.Position, map.Camera.Zoom + 1);

			var update = CameraUpdate.SetCamera(newCamera);
			clusterManager.Cluster();
			map.MoveCamera(update);
		}

		[Export("renderClusters:")]
		void RenderClusters(IGMUCluster[] clusters)
		{
			
		}

	}
}
