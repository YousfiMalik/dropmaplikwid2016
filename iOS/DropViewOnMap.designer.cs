// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace ClusteringMapXamarinForms.iOS
{
    [Register ("DropViewOnMap")]
    partial class DropViewOnMap
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel description { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView emoji { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView thumbnail { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (description != null) {
                description.Dispose ();
                description = null;
            }

            if (emoji != null) {
                emoji.Dispose ();
                emoji = null;
            }

            if (thumbnail != null) {
                thumbnail.Dispose ();
                thumbnail = null;
            }
        }
    }
}